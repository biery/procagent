import psutil
import time

class DiskMonitor:

    def __init__(self):
        self.disk_percent = 0.0

    def startAgent(self):
        while True:
            curr_disk_usage = self.getDiskUsage()
            self.disk_percent = curr_disk_usage.percent
            time.sleep(3)

    def getDiskUsage(self):
        return psutil.disk_usage('/')