import psutil
import time
known_good = ["wininit.exe","winlogon.exe","explorer.exe","cmd.exe"]

class ProcMonitor(object):
    def __init__(self):
        self.total_processes = 0
        self.known_processes = 0
        self.unknown_processes = 0

    def startAgent(self):
        while True:
            self.total_processes = 0
            self.known_processes = 0
            self.unknown_processes = 0
            for curr_process in psutil.process_iter():
                process_name = curr_process.name()
                if process_name in known_good:
                    self.known_processes +=1
                else:
                    self.unknown_processes +=1
                self.total_processes = self.known_processes + self.unknown_processes
            time.sleep(3)

    def terminateProcess(self, pid):
        process = psutil.Process(pid)
        process_name = process.name()
        try:
            parent_process = psutil.Process(process.ppid())
        except:
            print("No Parent Process Found")
            parent_process = ""
        if process_name not in known_good:
            process.terminate()
            print("Process(es) " + str(pid) + " Terminated")
            if parent_process != "" and parent_process.name not in known_good:
                parent_process.terminate()
                print("Process Parent Terminated: " + str(parent_process.pid))

    def killCPUHog(self):
        high_proc_name = ""
        high_pid = -1
        high_percent = 0.0
        for process in psutil.process_iter():
            tmp_percent = process.cpu_percent()
            if tmp_percent > high_percent:
                high_percent = tmp_percent
                high_pid = process.pid
                high_proc_name = process.name()
        if high_proc_name != "" and high_pid != -1:
            print("\nExecuting Rule: If High CPU Usage Destroy Top Unknown Process")
            print("Target: " + high_proc_name)
            print("Target PID: " + str(high_pid))
            if high_proc_name not in known_good:
                self.terminateProcess(high_pid)


