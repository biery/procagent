import psutil
import time


class CpuMonitor(object):
    def __init__(self):
        self.user_cpu_percent = 0.0
        self.system_cpu_percent = 0.0
        self.total_cpu_percent = 0.0

    def startAgent(self):
        while True:
            curr_cpu_stats = self.getCPUStats(None)
            self.user_cpu_percent = curr_cpu_stats.user
            self.system_cpu_percent = curr_cpu_stats.system
            self.total_cpu_percent = 100.0 - curr_cpu_stats.idle

            time.sleep(3)

    def getCPUStats(self, interval):
        return psutil.cpu_times_percent(interval=interval, percpu=False)