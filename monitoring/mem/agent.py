import psutil
import time


class MemMonitor:
    def __init__(self):
        self.mem_percent = 0.0
        self.swap_percent = 0.0

    def startAgent(self):
        while True:
            curr_mem = self.getMemStats()
            self.mem_percent = curr_mem.percent
            curr_swap = self.getSwapStats()
            self.swap_percent = curr_swap.percent
            time.sleep(2)

    def getMemStats(self):
        return psutil.virtual_memory()

    def getSwapStats(self):
        return psutil.swap_memory()
