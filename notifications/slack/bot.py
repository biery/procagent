import urllib.request, urllib.parse
import json

class SlackBot:
    def __init__(self):
        self.url = 'https://hooks.slack.com/services/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'

    def sendMessage(self, message):
        header = {"Content-Type": "application/json"}
        payload={"text": message}

        data = json.dumps(payload)
        req = urllib.request.Request(self.url, bytes(data.encode('utf-8')))
        resp = urllib.request.urlopen(req)
