import sys
sys.path.append("\procagent")
import time
import threading
import os
import signal
from monitoring.cpu.agent import CpuMonitor
from monitoring.mem.agent import MemMonitor
from monitoring.disk.agent import DiskMonitor
from monitoring.process.agent import ProcMonitor
from notifications.slack.bot import SlackBot

#Instances of each agent in memory
cpuLoop = CpuMonitor()
diskLoop = DiskMonitor()
memLoop = MemMonitor()
processLoop = ProcMonitor()
slackBot = SlackBot()
#Aggregated Facts
highCPUUsage = False
highMemUsage = False
highSwapUsage = False
highDiskUsage = False
#Iterate and perform actions based on the state of the system
def stateLoop():
    print("---Initializing state of machine---")
    time.sleep(5)
    #Clear the screen so that only the instance manager is seen.
    clear = lambda: os.system('cls')
    clear()
    cycle = 0
    while True:
        print("State Cycle Iteration: " + str(cycle))
        print("--------------")
        print("CPU Usage User: " + str(cpuLoop.user_cpu_percent))
        print("CPU Usage System: " + str(cpuLoop.system_cpu_percent))
        print("CPU Usage Total: %0.2f" % cpuLoop.total_cpu_percent)
        print("--------------")
        print("Memory Usage Percent: " + str(memLoop.mem_percent))
        print("Swap Usage: " + str(memLoop.swap_percent))
        print("--------------")
        print("Disk Usage Percent: " + str(diskLoop.disk_percent))
        print("--------------")
        print("Known Processes: " + str(processLoop.known_processes))
        print("Unknown Processes: " + str(processLoop.unknown_processes))
        print("--------------")
        runAggregatedFacts()
        print("\nAggregated Facts")
        print("High CPU Usage:", highCPUUsage)
        print("High Memory Usage:", highMemUsage)
        print("High Swap Usage:", highSwapUsage)
        print("High Disk Usage:", highDiskUsage)
        time.sleep(5)
        clear = lambda: os.system('cls')
        clear()
        cycle += 1

# take machine state and create facts from the state
def runAggregatedFacts():
    global highCPUUsage
    global highMemUsage
    global highSwapUsage
    global highDiskUsage
    if cpuLoop.total_cpu_percent > 90.0:
        highCPUUsage = True
    else:
        highCPUUsage = False
    if memLoop.mem_percent > 70.0:
        highMemUsage = True
    else:
        highMemUsage = False
    if memLoop.swap_percent > 40.0:
        highSwapUsage = True
    else:
        highSwapUsage = False
    if diskLoop.disk_percent > 85.0:
        highDiskUsage = True
    else:
        highDiskUsage = False

def runExecutioner():
    while True:
        if (highCPUUsage):
            slackBot.sendMessage("High CPU Usage Detected, Executing Kill Function on Unknown Processes")
            processLoop.killCPUHog()
        time.sleep(5)

#Signal Handler catches Ctrl+C command on command line
def signal_handler(signal, frame):
    print ("Ctrl+C received")
    sys.exit(0)

#Setup handler for SIGINT
def setup_signal():
    signal.signal(signal.SIGINT, signal_handler)

if __name__ == '__main__':
    #CPU Agent daemon thread
    cpuThread = threading.Thread(target=cpuLoop.startAgent)
    cpuThread.daemon = True
    cpuThread.start()
    #Memory Agent daemon thread
    memThread = threading.Thread(target=memLoop.startAgent)
    memThread.daemon = True
    memThread.start()
    #Disk Agent daemon thread
    diskThread = threading.Thread(target=diskLoop.startAgent)
    diskThread.daemon = True
    diskThread.start()
    #Process Agent daemon thread
    processThread = threading.Thread(target=processLoop.startAgent)
    processThread.daemon = True
    processThread.start()
    #Executer thread
    AgentThread = threading.Thread(target=stateLoop)
    AgentThread.daemon = True
    AgentThread.start()
    #Setting up main thread to catch terminate signal
    ExecutionerThread = threading.Thread(target=runExecutioner)
    ExecutionerThread.daemon = True
    ExecutionerThread.start()
    setup_signal()
    while True:
        time.sleep(1)



